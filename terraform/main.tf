provider "aws" {
  region = "eu-west-1"
  s3_use_path_style = true
}

provider "aws" {
  alias = "us-east-1"
  region = "us-east-1"
}

locals {
  bucket_name="jagadeeshreddy.net"
}

resource "aws_s3_bucket" "deploy-bucket" {
  bucket = local.bucket_name
}

resource "aws_s3_bucket_website_configuration" "website" {
  bucket = aws_s3_bucket.deploy-bucket.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "index.html"
  }
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.deploy-bucket.bucket
  policy = file("./s3-allow-public-access-policy.json")
}

data "aws_acm_certificate" "jagadeeshreddy" {
  provider    = aws.us-east-1
  domain      = "jagadeeshreddy.net"
  types       = ["AMAZON_ISSUED"]
  most_recent = true
}

resource "aws_cloudfront_distribution" "portfolio" {
  enabled = true
  aliases = ["jagadeeshreddy.net"]
  price_class = "PriceClass_100"
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id       = aws_s3_bucket_website_configuration.website.website_endpoint
    viewer_protocol_policy = "redirect-to-https"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }
  origin {
    domain_name = aws_s3_bucket.deploy-bucket.bucket_regional_domain_name
    origin_id   = aws_s3_bucket_website_configuration.website.website_endpoint
  }
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  viewer_certificate {
    acm_certificate_arn = data.aws_acm_certificate.jagadeeshreddy.arn
    ssl_support_method = "sni-only"
    minimum_protocol_version = "TLSv1.2_2021"
  }
}

data "aws_route53_zone" "primary" {
  name = "jagadeeshreddy.net"
}

resource "aws_route53_record" "portfolio" {
  name = "jagadeeshreddy.net"
  type = "A"
  zone_id = data.aws_route53_zone.primary.zone_id

  alias {
    evaluate_target_health = true
    name                   = aws_cloudfront_distribution.portfolio.domain_name
    zone_id                = aws_cloudfront_distribution.portfolio.hosted_zone_id
  }
}
