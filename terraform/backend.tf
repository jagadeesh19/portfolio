terraform {
  backend "s3" {
    bucket = "personal-sre-tf"
    key = "portfolio/terraform.tfstate"
    region = "eu-west-1"
  }
}
