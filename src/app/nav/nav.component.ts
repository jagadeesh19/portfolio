import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.less']
})
export class NavComponent implements OnInit {
  copied:boolean=false;
  
  constructor() { }

  ngOnInit(): void {
  }
  
  onClick (){
    this.copied=true;
    navigator.clipboard.writeText("yeturujagadeesh@gmail.com");
    setTimeout(()=>{
      this.copied=false;
      
    }, 1000);
  }

}
