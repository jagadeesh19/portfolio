import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mobile-nav',
  templateUrl: './mobile-nav.component.html',
  styleUrls: ['./mobile-nav.component.less']
})
export class MobileNavComponent implements OnInit {
  clicked: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }
  
  onScroll(){
    setTimeout(()=>this.clicked=!this.clicked,100);
  }

}
